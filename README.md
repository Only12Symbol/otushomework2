# OtusHomeWork2

Создаём набор классов и их интерфейсов

Задание 1:
Реализация IEnumerable<T> на примере чтения списка элементов из xml, json ил сsv файла.
В качестве десериализатора можно использовать https://github.com/ExtendedXmlSerializer/home или http://csvhelper.com/

Здесь более пошагово на примере xml и некоего класса Person:
https://pastebin.com/6FJZanYv


Задание 2:
Создать интерфейс IAlgorithm, добавить в него метод. Например, сортировка. Применить интерфейс к классу из первого задания.
(Помните, что реализовав IEnumerable<T>, для вашего класса становятся доступны методы LINQ?)

Задание 3:
Реализуйте интерфейсы из https://gist.github.com/viktor-nikolaev/46e588fc1c52bbf03186597af23fcd61
Напишите тесты IAccountService.AddAcount() с использованием Moq.